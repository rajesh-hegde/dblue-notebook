import json
import os
import re
import urllib.request

from notebook.auth.login import LoginHandler as NotebookLoginHandler


class LoginHandler(NotebookLoginHandler):
    endpoint = os.environ.get("DBLUE_AUTH_ENDPOINT")

    @classmethod
    def get_user_token(cls, handler):
        """Identify the user based on a token in the URL or Authorization header

        Returns:
        - uuid if authenticated
        - None if not
        """

        if not LoginHandler.endpoint:
            raise Exception("Define auth endpoint: export DBLUE_AUTH_ENDPOINT=https://{host}:{port}/api/v1/users/current")

        # check login token from URL argument or Authorization header
        access_token = cls.get_token(handler)

        req = urllib.request.Request(LoginHandler.endpoint)
        req.add_header('Authorization', 'Bearer %s' % access_token)

        with urllib.request.urlopen(req) as f:
            user = f.read().decode('utf-8')
            if user:
                return json.loads(user)["username"]

        return None

    @classmethod
    def get_token(cls, handler):
        """Get the user token from a request

        Default:

        - in URL parameters: ?access_token=<token>
        - in header: Authorization: Bearer <token>
        - in cookie: access_token=<token>
        """

        def _get_token_from_argument():
            return handler.get_argument('access_token', '')

        def _get_token_from_header():
            # get it from Authorization header
            auth_header = handler.request.headers.get('Authorization', '')
            auth_header_pattern = re.compile('Bearer\s+(.+)', re.IGNORECASE)

            m = auth_header_pattern.match(auth_header)
            if m:
                return m.group(1)

        def _get_token_from_cookie():
            return handler.get_cookie("access_token")

        access_token = _get_token_from_argument() or _get_token_from_header() or _get_token_from_cookie()

        return access_token or ''
