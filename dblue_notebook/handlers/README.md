### Dblue notebook handlers

#### Install

#### Config

```
export DBLUE_AUTH_ENDPOINT=http://127.0.0.1:8000/api/v1/users/current
```

##### Update notebook config

- Override in `jupyter_notebook_config.py`

```
c.NotebookApp.login_handler_class = 'dblue_notebook.handlers.login.LoginHandler'
```

- Pass to jupyterlab command

```
jupyter lab --NotebookApp.login_handler_class='dblue_notebook.handlers.login.LoginHandler'
```
