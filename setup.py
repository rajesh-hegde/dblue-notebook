import io
import os
import sys
from shutil import rmtree

from setuptools import find_packages, setup, Command

# Package meta-data.
NAME = "dblue_notebook"
DESCRIPTION = "Dblue Notebook "
URL = "https://bitbucket.org/rajesh-hegde/dblue-notebook"
EMAIL = "rajesh@dblue.ai"
AUTHOR = "Rajesh Hegde"
REQUIRES_PYTHON = ">=3.7.0"
VERSION = "1.1.0"

# Required packages
REQUIRED = ["notebook>=4.3.1", "gcsfs==0.2.3"]

# Optional packages
EXTRAS = {
    # 'fancy feature': ['django'],
}

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(exclude=["tests", "*.tests", "*.tests.*", "tests.*"]),
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license=None,
    classifiers=[],
)
